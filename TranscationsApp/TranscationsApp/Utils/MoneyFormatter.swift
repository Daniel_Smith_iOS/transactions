//  Created by Daniel on 12.10.2020.

import Foundation

enum MoneyFormatter {
    static func makeMoneyString(from value: Double) -> String {
        let number = NSNumber(value: value)
        let str = NumberFormatter.transactionFormatter.string(from: number) ?? ""
        if value > 0 {
            return "+" + str
        }
        return str
    }
}
