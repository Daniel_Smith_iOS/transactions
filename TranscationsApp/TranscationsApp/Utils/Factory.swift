//  Created by Daniel on 09.10.2020.

import Foundation
import Apollo

struct Factory {
    static let shared = Factory()
    
    private static var network: Network?
    func make() -> Network {
        if let network = Self.network { return network }
        let network = ApolloBasedNetwork(
            apolloClient: ApolloClient(url: URL(string: "http://localhost:4000/graphql")!))
        Self.network = network
        return network
    }
    
    private weak static var transcationsService: TransactionsService?
    func make() -> TransactionsService {
        if let service = Self.transcationsService { return service }
        let service = TransactionsServiceNetworkBased(network: self.make())
        Self.transcationsService = service
        return service
    }
}
