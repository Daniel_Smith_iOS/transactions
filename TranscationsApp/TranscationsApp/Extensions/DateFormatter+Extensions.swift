//  Created by Daniel on 09.10.2020.

import Foundation

extension DateFormatter {
    static let YYYY_mm_ddd: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-mm-dd"
        return formatter
    }()
    
    static let MMMM_dd: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM, dd"
        return dateFormatter
    }()

}
