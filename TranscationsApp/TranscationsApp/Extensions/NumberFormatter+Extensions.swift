//  Created by Daniel on 12.10.2020.

import Foundation

extension NumberFormatter {
    static let transactionFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.maximumFractionDigits = 2
        formatter.locale = Locale(identifier: "en_GB")
        return formatter
    }()
}
