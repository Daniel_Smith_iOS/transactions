//  Created by Daniel on 12.10.2020.

import Foundation
import SwiftUI

extension Color {
    /// RGB 0, 0, 0
    static let n0 = Color.black
    /// RGB 255, 255, 255
    static let n1 = Color.white
    /// RGB: 246, 249, 253
    static let n2 = Color(#colorLiteral(red: 0.9647058824, green: 0.9764705882, blue: 0.9921568627, alpha: 1))
    static let n3 = Color(#colorLiteral(red: 0.631372549, green: 0.6588235294, blue: 0.6705882353, alpha: 1))
    /// RGB: 105, 138,  63
    static let n5 = Color(#colorLiteral(red: 0.4117647059, green: 0.5411764706, blue: 0.2470588235, alpha: 1))
    /// RGB: 241, 249, 232
    static let n6 = Color(#colorLiteral(red: 0.9450980392, green: 0.9764705882, blue: 0.9098039216, alpha: 1))
        

}
