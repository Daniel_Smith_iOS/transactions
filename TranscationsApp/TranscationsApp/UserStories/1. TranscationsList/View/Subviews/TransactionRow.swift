//  Created by Daniel on 10.10.2020.

import Foundation
import SwiftUI

struct TransactionRow: View {
    let transaction: Transaction
    
    private var bodyBackground: Color {
        switch transaction.type {
        case .cashback:
            return .n6
        case .regular:
            return .n1
        }
    }
    
    private var titleColor: Color {
        switch transaction.type {
        case .cashback:
            return .n5
        case .regular:
            return .n0
        }
    }
    
    var body: some View {
        bodyBackground
            .overlay(
                HStack(alignment: .center, spacing: 0.0) {
                    Image(transaction.iconName)
                        .resizable()
                        .frame(width: 40, height: 40)
                        .cornerRadius(14.0)
                    Text(transaction.title)
                        .foregroundColor(titleColor)
                        .padding(.leading, 16)
                    Spacer()
                    Text(MoneyFormatter.makeMoneyString(from: transaction.amount.value))
                        .foregroundColor(titleColor)
                }
                .padding(EdgeInsets(top: 16, leading: 16, bottom: 16, trailing: 16))
            )
            .cornerRadius(12)
            .frame(height: 70)
    }
}

struct TransactionRow_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            TransactionRow(transaction: Transaction(
                            id: "1",
                            title: "Nando's",
                            iconName: "restaurant",
                            amount: Money(value: 25.2,
                                          currency: .GBR),
                            type: .regular))
                .previewLayout(.fixed(width: 300, height: 70))
            
            TransactionRow(transaction: Transaction(
                            id: "2",
                            title: "Hard rock cafe",
                            iconName: "cashback",
                            amount: Money(value: 0.03,
                                          currency: .GBR),
                            type: .cashback))
                .previewLayout(.fixed(width: 300, height: 70))
        }
    }
}
