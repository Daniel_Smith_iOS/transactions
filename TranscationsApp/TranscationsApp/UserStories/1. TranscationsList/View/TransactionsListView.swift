//  Created by Daniel on 10.10.2020.

import Foundation
import SwiftUI

struct SectionHeader: View {
    
    let date: Date
    let amount: Money
    
    var body: some View {
        HStack(alignment: .center, spacing: 0) {
            Text(DateFormatter.MMMM_dd.string(from: date))
                .foregroundColor(Color.n3)
            Spacer()
            Text(MoneyFormatter.makeMoneyString(from: self.amount.value))
                .font(Font.system(size: 16))
                .fontWeight(.semibold)
        }
        .background(Color.white)
    }
}

struct TransactionsListView: View {
    @ObservedObject var viewModel: TranscationsListViewModel
    
    var body: some View {
        List(viewModel.transcations) { tGroup in
            VStack(alignment: .leading, spacing: 0) {
                SectionHeader(date: tGroup.date, amount: tGroup.amount)
                    .padding(EdgeInsets(top: 24, leading: 16, bottom: 16, trailing: 16))
                
                ForEach(0..<tGroup.transcations.count, id: \.self) { tIndex in
                    let transaction = tGroup.transcations[tIndex]
                    TransactionRow(transaction: transaction)
                }
                Rectangle()
                    .fill(Color.n2)
                    .frame(height: 10)
                    .padding(EdgeInsets(top: 0, leading: -32, bottom: -32, trailing: -32))
            }
        }
        .alert(isPresented: $viewModel.isErrorShown, content: {
            Alert(title: Text("Error"),
                  message: Text(viewModel.errorString),
                  dismissButton: .default(Text("OK"), action: {
                    self.viewModel.onErrorAlertTap()
                  }))
        })
    }
}

struct TransactionsView_Previews: PreviewProvider {
    static var previews: some View {
        TransactionsListView(viewModel: TranscationsListViewModel(transactionsService: Factory.shared.make()))
    }
}
