//  Created by Daniel on 09.10.2020.

import Foundation
import Combine
import SwiftUI

final class TranscationsListViewModel: ObservableObject {
    @Published var transcations: [TransactionsGroup] = []
    @Published var isLoading: Bool = false
    @Published var isErrorShown: Bool = false
    @Published var errorString = "Kek"
    
    private let transactionsService: TransactionsService
    private var request: AnyCancellable?
    
    init(transactionsService: TransactionsService) {
        self.transactionsService = transactionsService
        self.request = transactionsService
            .getDailyTransactionGroups()
            .receive(on: RunLoop.main)
            .sink(
                receiveCompletion: { [weak self] completion in
                    guard let self = self,
                          case let .failure(error) = completion else { return }
                    self.errorString = error.localizedDescription
                    self.isErrorShown = true
                },
                receiveValue: { [weak self] result in
                    self?.transcations = result
                })
    }
    
    func onErrorAlertTap() {
        self.isErrorShown = false
    }
}
