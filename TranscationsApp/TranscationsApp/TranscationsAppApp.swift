//  Created by Daniel on 09.10.2020.

import SwiftUI

@main
struct TranscationsAppApp: App {
    var body: some Scene {
        WindowGroup {
            TransactionsListView(viewModel: TranscationsListViewModel(transactionsService: Factory.shared.make()))
        }
    }
}
