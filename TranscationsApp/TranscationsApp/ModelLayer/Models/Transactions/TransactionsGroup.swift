//  Created by Daniel on 09.10.2020.

import Foundation

struct TransactionsGroup: Identifiable {
    var id: TimeInterval { self.date.timeIntervalSince1970 }
    let date: Date
    let amount: Money
    let transcations: [Transaction]
}
