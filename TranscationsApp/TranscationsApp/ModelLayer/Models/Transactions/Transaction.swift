//  Created by Daniel on 09.10.2020.

import Foundation

struct Transaction: Identifiable {
    enum TransactionType {
        case regular, cashback
    }
    let id: String
    let title: String
    let iconName: String
    let amount: Money
    let type: TransactionType
}
