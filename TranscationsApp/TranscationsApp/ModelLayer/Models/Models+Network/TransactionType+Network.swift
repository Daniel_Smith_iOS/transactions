//  Created by Daniel on 09.10.2020.

import Foundation

extension Transaction.TransactionType {
    init?(transactionTypeAPI: TransactionType) {
        switch transactionTypeAPI {
        case .regular:
            self = .regular
        case .cashback:
            self = .cashback
        case .__unknown(let value):
            debugPrint("⚠️ Not impement transaction type \(value)")
            return nil
        }
    }
}
