//  Created by Daniel on 09.10.2020.

import Foundation

typealias TransactionAPI = TranscationsQuery.Data.DailyTransactionsFeed.AsTransactionWidget

extension Transaction {
    init?(transactionAPI: TransactionAPI) {
        let source = transactionAPI.transaction
        guard let amount = Money(moneyAPI: source.amount),
              let type = TransactionType(transactionTypeAPI: source.type) else { return nil }
        self.id = source.id
        self.title = source.title
        self.iconName = transactionAPI.image?.iconName ?? ""
        self.amount = amount
        self.type = type
    }
}
