//  Created by Daniel on 09.10.2020.

import Foundation

extension Currency {
    
    init?(currencyAPI: CurrencyCode) {
        switch currencyAPI {
        case .gbp:
            self = .GBR
        case .rub:
            self = .RUB
        case .usd:
            self = .USD
        case .__unknown(let code):
            debugPrint("⚠️ Not impement currency type \(code)")
            return nil
        }
    }
}
