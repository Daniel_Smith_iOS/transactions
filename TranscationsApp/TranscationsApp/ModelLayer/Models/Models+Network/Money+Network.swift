//  Created by Daniel on 09.10.2020.

import Foundation

protocol MoneyAPI {
    var value: String { get }
    var currencyCode: CurrencyCode { get }
}

extension TranscationsQuery.Data.DailyTransactionsFeed.AsDaySectionWidget.Amount: MoneyAPI {}
extension TranscationsQuery.Data.DailyTransactionsFeed.AsTransactionWidget.Transaction.Amount: MoneyAPI {}

extension Money {
    init?(moneyAPI: MoneyAPI) {
        guard let value = Double(moneyAPI.value) else {
            debugPrint("⚠️ Can't convet amount of \(moneyAPI.value) to Double")
            return nil
        }
        guard let currency = Currency(currencyAPI: moneyAPI.currencyCode) else { return nil }
        self.value = value
        self.currency = currency
    }
}
