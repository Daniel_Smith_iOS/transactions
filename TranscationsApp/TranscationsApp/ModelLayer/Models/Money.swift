//  Created by Daniel on 09.10.2020.

import Foundation

struct Money {
    let value: Double
    let currency: Currency
}
