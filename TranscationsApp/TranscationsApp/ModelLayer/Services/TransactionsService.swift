//  Created by Daniel on 09.10.2020.

import Foundation
import Combine

typealias TranscationsFuture = Publishers.Map<Future<TranscationsQuery.Data, NetworkError>, [TransactionsGroup]>
protocol TransactionsService: AnyObject {
    func getDailyTransactionGroups() -> TranscationsFuture
}

final class TransactionsServiceNetworkBased: TransactionsService {
    
    private let network: Network
    
    init(network: Network) {
        self.network = network
    }
    
    func getDailyTransactionGroups() -> TranscationsFuture {
        return self.network.fetch(TranscationsQuery())
            .map { Self.map(feed: $0.dailyTransactionsFeed?.compactMap { $0 } ?? []) }
    }
    
    private static func map(feed: [TranscationsQuery.Data.DailyTransactionsFeed]) -> [TransactionsGroup] {
        var result = [TransactionsGroup]()
        
        var pendingGroup: (date: Date, amount: Money)? = nil
        var pendingTransactions = [Transaction]()
        var fixMe = 0
        feed.forEach { feedItem in
            
            if let daySection = feedItem.asDaySectionWidget {
                if let group = pendingGroup {
                    result.append(TransactionsGroup(date: group.date,
                                                    amount: group.amount,
                                                    transcations: pendingTransactions))
                }
                pendingGroup = nil
                pendingTransactions = []
                
                guard let date = DateFormatter.YYYY_mm_ddd.date(from: daySection.date),
                      let amount = Money(moneyAPI: daySection.amount) else { return }
                fixMe += 1
                pendingGroup = (date: date, amount: amount)
                
            } else if let transcationAPI = feedItem.asTransactionWidget,
                      let transaction = Transaction(transactionAPI: transcationAPI) {
                pendingTransactions.append(transaction)
            }
            
        }
        
        return result
    }
}
