//  Created by Daniel on 09.10.2020.

import Foundation
import Apollo
import Combine

enum NetworkError: Error {
    case unowned(Error)
    case graphQL([GraphQLError])
    case noData
}

protocol Network: AnyObject {
    func fetch<T: GraphQLQuery>(_ query: T) -> Future<T.Data, NetworkError>
}

final class ApolloBasedNetwork: Network {
    
    private let apolloClient: ApolloClient
    
    init(apolloClient: ApolloClient) {
        self.apolloClient = apolloClient
    }
    
    func fetch<T: GraphQLQuery>(_ query: T) -> Future<T.Data, NetworkError> {
        
        return Future<T.Data, NetworkError> { [unowned self] promise in
            
            self.apolloClient.fetch(query: query) { result in
                switch result {
                case .success(let graphQLResult):
                    if let data = graphQLResult.data {
                        promise(.success(data))
                    } else if let errors = graphQLResult.errors {
                        promise(.failure(.graphQL(errors)))
                    } else {
                        promise(.failure(.noData))
                    }
                case .failure(let error):
                    promise(.failure(.unowned(error)))
                }
            }
        }
    }
}
